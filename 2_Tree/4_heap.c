#include <stdio.h>
#include <stdlib.h>

#define INT_MIN = -99999;

// Structure to represent a min-heap
struct MinHeap {
    int* array;
    int capacity;
    int size;
};

// Function to initialize a min-heap
struct MinHeap* createMinHeap(int capacity) {
    struct MinHeap* minHeap = (struct MinHeap*)malloc(sizeof(struct MinHeap));
    if (!minHeap) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }
    minHeap->capacity = capacity;
    minHeap->size = 0;
    minHeap->array = (int*)malloc(capacity * sizeof(int));
    if (!minHeap->array) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }
    return minHeap;
}

// Function to swap two integers
void swap(int* a, int* b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

// Function to heapify a subtree rooted with the given index
void heapify(struct MinHeap* minHeap, int index) {
    int smallest = index;
    int leftChild = 2 * index + 1;
    int rightChild = 2 * index + 2;

    // Compare with left child
    if (leftChild < minHeap->size && minHeap->array[leftChild] < minHeap->array[smallest])
        smallest = leftChild;

    // Compare with right child
    if (rightChild < minHeap->size && minHeap->array[rightChild] < minHeap->array[smallest])
        smallest = rightChild;

    // If the smallest is not the root, swap and continue heapifying
    if (smallest != index) {
        swap(&minHeap->array[index], &minHeap->array[smallest]);
        heapify(minHeap, smallest);
    }
}

// Function to insert a new key to the min-heap
void insertKey(struct MinHeap* minHeap, int key) {
    if (minHeap->size == minHeap->capacity) {
        printf("Heap overflow. Cannot insert key %d.\n", key);
        return;
    }

    // Insert the new key at the end
    int currentIndex = minHeap->size;
    minHeap->array[currentIndex] = key;
    minHeap->size++;

    // Fix the min-heap property if violated
    while (currentIndex != 0 && minHeap->array[(currentIndex - 1) / 2] > minHeap->array[currentIndex]) {
        swap(&minHeap->array[currentIndex], &minHeap->array[(currentIndex - 1) / 2]);
        currentIndex = (currentIndex - 1) / 2;
    }
}

// Function to decrease the value of a key at a given index
void decreaseKey(struct MinHeap* minHeap, int index, int newValue) {
    if (index >= minHeap->size) {
        printf("Invalid index %d.\n", index);
        return;
    }

    minHeap->array[index] = newValue;

    // Fix the min-heap property if violated
    while (index != 0 && minHeap->array[(index - 1) / 2] > minHeap->array[index]) {
        swap(&minHeap->array[index], &minHeap->array[(index - 1) / 2]);
        index = (index - 1) / 2;
    }
}

// Function to extract the minimum value from the min-heap
int extractMin(struct MinHeap* minHeap) {
    if (minHeap->size <= 0) {
        printf("Heap underflow. Cannot extract minimum value.\n");
        return -1;
    }

    if (minHeap->size == 1) {
        minHeap->size--;
        return minHeap->array[0];
    }

    // Store the minimum value and remove it from the heap
    int minValue = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
    minHeap->size--;

    // Fix the min-heap property if violated
    heapify(minHeap, 0);

    return minValue;
}

// Function to delete a key at a given index from the min-heap
void deleteKey(struct MinHeap* minHeap, int index) {
    if (index >= minHeap->size) {
        printf("Invalid index %d.\n", index);
        return;
    }

    // Decrease the key value to minus infinity
    decreaseKey(minHeap, index, INT_MIN);

    // Extract the minimum value (which is now at the root)
    extractMin(minHeap);
}

// Function to display the elements of the min-heap
void display(struct MinHeap* minHeap) {
    if (minHeap->size == 0) {
        printf("Min-heap is empty.\n");
        return;
    }

    printf("Min-Heap elements: ");
    for (int i = 0; i < minHeap->size; i++) {
        printf("%d ", minHeap->array[i]);
    }
    printf("\n");
}

int main() {
    struct MinHeap* minHeap = createMinHeap(10);

    insertKey(minHeap, 3);
    insertKey(minHeap, 2);
    insertKey(minHeap, 1);
    insertKey(minHeap, 15);
    insertKey(minHeap, 5);
    insertKey(minHeap, 4);
    insertKey(minHeap, 45);

    printf("After insertion: ");
    display(minHeap);

    decreaseKey(minHeap, 2, 1);
    printf("After decreasing key at index 2 to 1: ");
    display(minHeap);

    printf("Extracted min value: %d\n", extractMin(minHeap));
    printf("After extracting min value: ");
    display(minHeap);

    deleteKey(minHeap, 1);
    printf("After deleting key at index 1: ");
    display(minHeap);

    return 0;
}

