#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Structure to represent a node in the adjacency list
struct Node {
    int data;
    struct Node* next;
};

// Structure to represent the adjacency list for each vertex
struct AdjList {
    struct Node* head;
};

// Structure to represent a graph
struct Graph {
    int vertices;
    struct AdjList* array;
};

// Function to create a new node
struct Node* createNode(int data) {
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    if (!newNode) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }
    newNode->data = data;
    newNode->next = NULL;
    return newNode;
}

// Function to create a graph with a given number of vertices
struct Graph* createGraph(int vertices) {
    struct Graph* graph = (struct Graph*)malloc(sizeof(struct Graph));
    if (!graph) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }
    graph->vertices = vertices;

    // Create an array of adjacency lists
    graph->array = (struct AdjList*)malloc(vertices * sizeof(struct AdjList));
    if (!graph->array) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }

    // Initialize each adjacency list as empty
    for (int i = 0; i < vertices; ++i) {
        graph->array[i].head = NULL;
    }

    return graph;
}

// Function to add an edge to the graph
void addEdge(struct Graph* graph, int src, int dest) {
    // Add an edge from src to dest
    struct Node* newNode = createNode(dest);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;

    // Add an edge from dest to src (assuming undirected graph)
    newNode = createNode(src);
    newNode->next = graph->array[dest].head;
    graph->array[dest].head = newNode;
}

// Function to perform Depth-First Search (DFS) starting from a given vertex
void dfsUtil(struct Graph* graph, int vertex, bool* visited) {
    visited[vertex] = true;
    printf("%d ", vertex);

    struct Node* current = graph->array[vertex].head;
    while (current != NULL) {
        if (!visited[current->data]) {
            dfsUtil(graph, current->data, visited);
        }
        current = current->next;
    }
}

void dfs(struct Graph* graph, int startVertex) {
    bool* visited = (bool*)malloc(graph->vertices * sizeof(bool));
    if (!visited) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < graph->vertices; ++i) {
        visited[i] = false;
    }

    printf("Depth-First Search (DFS) starting from vertex %d: ", startVertex);
    dfsUtil(graph, startVertex, visited);

    free(visited);
    printf("\n");
}

// Function to perform Breadth-First Search (BFS) starting from a given vertex
void bfs(struct Graph* graph, int startVertex) {
    bool* visited = (bool*)malloc(graph->vertices * sizeof(bool));
    if (!visited) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < graph->vertices; ++i) {
        visited[i] = false;
    }

    visited[startVertex] = true;

    // Create a queue for BFS
    int* queue = (int*)malloc(graph->vertices * sizeof(int));
    if (!queue) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }

    int front = -1;
    int rear = -1;

    printf("Breadth-First Search (BFS) starting from vertex %d: ", startVertex);

    // Enqueue the start vertex
    queue[++rear] = startVertex;

    while (front <= rear) {
        int currentVertex = queue[front++];
        printf("%d ", currentVertex);

        struct Node* current = graph->array[currentVertex].head;
        while (current != NULL) {
            if (!visited[current->data]) {
                visited[current->data] = true;
                queue[++rear] = current->data;
            }
            current = current->next;
        }
    }

    free(visited);
    free(queue);
    printf("\n");
}

int main() {
    // Create a graph with 5 vertices
    struct Graph* graph = createGraph(5);

    // Add edges to the graph
    addEdge(graph, 0, 1);
    addEdge(graph, 0, 4);
    addEdge(graph, 1, 2);
    addEdge(graph, 1, 3);
    addEdge(graph, 1, 4);
    addEdge(graph, 2, 3);
    addEdge(graph, 3, 4);

    // Perform DFS starting from vertex 0
    dfs(graph, 0);

    // Perform BFS starting from vertex 0
    bfs(graph, 0);

    return 0;
}

