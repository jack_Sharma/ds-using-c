#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/*
  The isalnum function in C is a standard library function
  that checks whether a given character is an alphanumeric
  character. In C, alphanumeric characters are letters
  (both uppercase and lowercase) and digits.
*/

// Structure to represent a stack
struct Stack {
    char items[100];
    int top;
};

// Function to initialize a stack
void initializeStack(struct Stack* stack) {
    stack->top = -1;
}

// Function to check if the stack is empty
int isEmpty(struct Stack* stack) {
    return stack->top == -1;
}

// Function to push a character onto the stack
void push(struct Stack* stack, char value) {
    stack->items[++stack->top] = value;
}

// Function to pop a character from the stack
char pop(struct Stack* stack) {
    if (!isEmpty(stack)) {
        return stack->items[stack->top--];
    }
    return '\0'; // Return null character if the stack is empty
}

// Function to get the precedence of an operator
int getPrecedence(char operator) {
    if (operator == '+' || operator == '-') {
        return 1;
    } else if (operator == '*' || operator == '/') {
        return 2;
    }
    return 0; // Lower precedence for other characters
}

// Function to convert infix to postfix
void infixToPostfix(char infix[], char postfix[]) {
    struct Stack operatorStack;
    initializeStack(&operatorStack);

    int i = 0; // Index for infix expression
    int j = 0; // Index for postfix expression

    while (infix[i] != '\0') {
        // Check if current element is operand
        if (isalnum(infix[i])) {
            postfix[j++] = infix[i++];
        } else if (infix[i] == '(') {
            push(&operatorStack, infix[i++]);
        } else if (infix[i] == ')') {
            while (!isEmpty(&operatorStack) && operatorStack.items[operatorStack.top] != '(') {
                postfix[j++] = pop(&operatorStack);
            }
            if (!isEmpty(&operatorStack) && operatorStack.items[operatorStack.top] == '(') {
                pop(&operatorStack); // Discard '('
            }
            i++;
        } else {
            while (!isEmpty(&operatorStack) && getPrecedence(infix[i]) <= getPrecedence(operatorStack.items[operatorStack.top])) {
                postfix[j++] = pop(&operatorStack);
            }
            push(&operatorStack, infix[i++]);
        }
    }

    // Pop any remaining operators from the stack
    while (!isEmpty(&operatorStack)) {
        postfix[j++] = pop(&operatorStack);
    }

    postfix[j] = '\0'; // Null-terminate the postfix expression
}

int main() {
    char infixExpression[100];
    char postfixExpression[100];

    printf("Enter an infix expression: ");
    scanf("%s", infixExpression);

    infixToPostfix(infixExpression, postfixExpression);

    printf("Postfix expression: %s\n", postfixExpression);

    return 0;
}

