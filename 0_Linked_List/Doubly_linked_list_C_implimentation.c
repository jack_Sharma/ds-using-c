#include <stdio.h>
#include <stdlib.h>

// Structure for a doubly linked list node
struct Node {
    int data;
    struct Node* prev;
    struct Node* next;
};

// Function to create a new node with the given data
struct Node* createNode(int data) {
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    if (!newNode) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }
    newNode->data = data;
    newNode->prev = newNode->next = NULL;
    return newNode;
}

// Function to insert a new node at the beginning of the doubly linked list
void insertAtBeginning(struct Node** head, int data) {
    struct Node* newNode = createNode(data);

    if (*head == NULL) {
        // If the list is empty, make the new node the head
        *head = newNode;
    } else {
        // Otherwise, adjust pointers and update the head
        newNode->next = *head;
        (*head)->prev = newNode;
        *head = newNode;
    }

    printf("Node with data %d inserted at the beginning.\n", data);
}

// Function to insert a new node at the end of the doubly linked list
void insertAtEnd(struct Node** head, int data) {
    struct Node* newNode = createNode(data);

    if (*head == NULL) {
        // If the list is empty, make the new node the head
        *head = newNode;
    } else {
        // Otherwise, traverse to the end and insert the new node
        struct Node* temp = *head;
        while (temp->next != NULL) {
            temp = temp->next;
        }
        temp->next = newNode;
        newNode->prev = temp;
    }

    printf("Node with data %d inserted at the end.\n", data);
}

// Function to delete a node from the beginning of the doubly linked list
void deleteFromBeginning(struct Node** head) {
    if (*head == NULL) {
        printf("List is empty. Cannot delete.\n");
        return;
    }

    struct Node* temp = *head;
    *head = temp->next;

    if (*head != NULL) {
        // If the new head is not NULL, update its previous pointer
        (*head)->prev = NULL;
    }

    free(temp);
    printf("Node deleted from the beginning.\n");
}

// Function to delete a node from the end of the doubly linked list
void deleteFromEnd(struct Node** head) {
    if (*head == NULL) {
        printf("List is empty. Cannot delete.\n");
        return;
    }

    struct Node* temp = *head;
    while (temp->next != NULL) {
        temp = temp->next;
    }

    if (temp->prev != NULL) {
        // If the node to delete is not the head, adjust pointers
        temp->prev->next = NULL;
    } else {
        // If the node to delete is the head, update the head
        *head = NULL;
    }

    free(temp);
    printf("Node deleted from the end.\n");
}

// Function to traverse and print the doubly linked list forward
void traverseForward(struct Node* head) {
    printf("Doubly Linked List (Forward): ");
    while (head != NULL) {
        printf("%d ", head->data);
        head = head->next;
    }
    printf("\n");
}

// Function to traverse and print the doubly linked list backward
void traverseBackward(struct Node* head) {
    printf("Doubly Linked List (Backward): ");
    while (head->next != NULL) {
        head = head->next;
    }
    while (head != NULL) {
        printf("%d ", head->data);
        head = head->prev;
    }
    printf("\n");
}

// Function to free the memory occupied by the doubly linked list
void freeList(struct Node** head) {
    struct Node* current = *head;
    struct Node* next;

    while (current != NULL) {
        next = current->next;
        free(current);
        current = next;
    }

    *head = NULL; // Set head to NULL after freeing all nodes
}

int main() {
    struct Node* myList = NULL;

    insertAtBeginning(&myList, 3);
    insertAtBeginning(&myList, 2);
    insertAtBeginning(&myList, 1);

    traverseForward(myList);
    traverseBackward(myList);

    insertAtEnd(&myList, 4);
    insertAtEnd(&myList, 5);

    traverseForward(myList);
    traverseBackward(myList);

    deleteFromEnd(&myList);

    traverseForward(myList);
    traverseBackward(myList);

    // Free memory
    freeList(&myList);

    return 0;
}

