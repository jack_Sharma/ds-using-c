#include <stdio.h>
#include <stdlib.h>

int main(){

  int n=5;

  int arr[n];

  for(int i=0;i<n;i++){
    arr[i] = i+1;   // 1 2 3 4 5 
  }

  // insertion at end:
  n++;
  arr[n] = 6;

  // deletion at any other index:
  for(int i=1;i<n;i++){       // O(n)
    arr[i] = arr[i+1];
  }

  n--;

  return 0;
}