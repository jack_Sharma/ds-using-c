#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 5

// Structure to represent a circular queue
struct CircularQueue {
    int items[MAX_SIZE];
    int front, rear;
};

// Function to initialize a circular queue
void initializeQueue(struct CircularQueue* queue) {
    queue->front = -1;
    queue->rear = -1;
}

// Function to check if the circular queue is empty
int isEmpty(struct CircularQueue* queue) {
    return (queue->front == -1 && queue->rear == -1);
}

// Function to check if the circular queue is full
int isFull(struct CircularQueue* queue) {
    return (queue->front == (queue->rear + 1) % MAX_SIZE);
}

// Function to enqueue (insert) an element into the circular queue
void enqueue(struct CircularQueue* queue, int value) {
    if (isFull(queue)) {
        printf("Queue overflow. Cannot enqueue %d.\n", value);
        return;
    }

    if (isEmpty(queue)) {
        queue->front = queue->rear = 0;
    } else {
        queue->rear = (queue->rear + 1) % MAX_SIZE;
    }

    queue->items[queue->rear] = value;
    printf("%d enqueued into the circular queue.\n", value);
}

// Function to dequeue (remove) an element from the circular queue
int dequeue(struct CircularQueue* queue) {
    if (isEmpty(queue)) {
        printf("Queue underflow. Cannot dequeue from an empty circular queue.\n");
        exit(EXIT_FAILURE);
    }

    int removedItem = queue->items[queue->front];

    if (queue->front == queue->rear) {
        // Last element in the circular queue
        queue->front = queue->rear = -1;
    } else {
        queue->front = (queue->front + 1) % MAX_SIZE;
    }

    return removedItem;
}

// Function to display the elements of the circular queue
void display(struct CircularQueue* queue) {
    if (isEmpty(queue)) {
        printf("Circular queue is empty.\n");
        return;
    }

    printf("Circular Queue elements: ");
    int i = queue->front;
    do {
        printf("%d ", queue->items[i]);
        i = (i + 1) % MAX_SIZE;
    } while (i != (queue->rear + 1) % MAX_SIZE);
    printf("\n");
}

int main() {
    struct CircularQueue myCircularQueue;
    initializeQueue(&myCircularQueue);

    enqueue(&myCircularQueue, 1);
    enqueue(&myCircularQueue, 2);
    enqueue(&myCircularQueue, 3);
    enqueue(&myCircularQueue, 4);

    display(&myCircularQueue);

    printf("Dequeued element: %d\n", dequeue(&myCircularQueue));

    display(&myCircularQueue);

    return 0;
}

