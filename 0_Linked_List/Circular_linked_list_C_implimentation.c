#include <stdio.h>
#include <stdlib.h>

// Structure for a node in the circular linked list
struct Node {
    int data;
    struct Node* next;
};

// Function to create a new node
struct Node* createNode(int data) {
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    if (!newNode) {
        printf("Memory allocation failed.\n");
        exit(EXIT_FAILURE);
    }
    newNode->data = data;
    newNode->next = NULL;
    return newNode;
}

// Function to insert a node at the beginning of the circular linked list
struct Node* insertAtBeginning(struct Node* head, int data) {
    struct Node* newNode = createNode(data);

    if (!head) {
        newNode->next = newNode; // The list is initially empty
    } else {
        newNode->next = head->next;
        head->next = newNode;
    }

    return newNode; // The new node becomes the new head
}

// Function to insert a node at the end of the circular linked list
struct Node* insertAtEnd(struct Node* head, int data) {
    struct Node* newNode = createNode(data);

    if (!head) {
        newNode->next = newNode; // The list is initially empty
        return newNode; // The new node becomes the head
    }

    newNode->next = head->next;
    head->next = newNode;

    return newNode; // The new node becomes the new tail
}

// Function to delete a node from the beginning of the circular linked list
struct Node* deleteFromBeginning(struct Node* head) {
    if (!head) {
        printf("List is empty. Cannot delete.\n");
        return NULL;
    }

    struct Node* removedNode = head->next;

    if (head->next == head) {
        // Only one node in the list
        free(removedNode);
        return NULL; // The list becomes empty
    }

    head->next = removedNode->next;
    free(removedNode);

    return head; // The next node becomes the new head
}

// Function to traverse and print the circular linked list
void traverse(struct Node* head) {
    if (!head) {
        printf("List is empty.\n");
        return;
    }

    struct Node* current = head->next;

    do {
        printf("%d ", current->data);
        current = current->next;
    } while (current != head->next);

    printf("\n");
}

// Function to free the memory occupied by the circular linked list
void freeList(struct Node* head) {
    if (!head) {
        return;
    }

    struct Node* current = head->next;
    struct Node* next;

    while (current != head) {
        next = current->next;
        free(current);
        current = next;
    }

    free(head);
}

int main() {
    struct Node* myList = NULL;

    // Insert nodes at the beginning
    myList = insertAtBeginning(myList, 3);
    myList = insertAtBeginning(myList, 2);
    myList = insertAtBeginning(myList, 1);

    // Print the list
    printf("Original List: ");
    traverse(myList);

    // Insert nodes at the end
    myList = insertAtEnd(myList, 4);
    myList = insertAtEnd(myList, 5);

    // Print the updated list
    printf("Updated List: ");
    traverse(myList);

    // Delete a node from the beginning
    myList = deleteFromBeginning(myList);

    // Print the list after deletion
    printf("List after deletion: ");
    traverse(myList);

    // Free the memory
    freeList(myList);

    return 0;
}

