#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 10

struct Queue {
    int front, rear;
    int array[MAX_SIZE];
};

// Function to initialize a queue
void initializeQueue(struct Queue* queue) {
    queue->front = -1;
    queue->rear = -1;
}

// Function to check if the queue is empty
int isEmpty(struct Queue* queue) {
    return (queue->front == -1);
}

// Function to check if the queue is full
int isFull(struct Queue* queue) {
    return (queue->rear + 1) == MAX_SIZE;
}

// Function to enqueue an element
void enqueue(struct Queue* queue, int value) {
    if (isFull(queue)) {
        printf("Queue is full. Cannot enqueue %d.\n", value);
        return;
    }

    if (isEmpty(queue)) {
        // If the queue is initially empty, set front to 0
        queue->front = 0;
    }

    // Increment rear and enqueue the element
    queue->rear++;
    queue->array[queue->rear] = value;

    printf("%d enqueued to the queue.\n", value);
}

// Function to dequeue an element
int dequeue(struct Queue* queue) {
    int value;

    if (isEmpty(queue)) {
        printf("Queue is empty. Cannot dequeue.\n");
        return -1; // Return an invalid value indicating failure
    }

    // Dequeue the element and increment front
    value = queue->array[queue->front];
    queue->front++;

    // If the last element is dequeued, reset front and rear
    if (queue->front > queue->rear) {
        initializeQueue(queue);
    }

    return value;
}

// Function to display the elements of the queue
void displayQueue(struct Queue* queue) {
    if (isEmpty(queue)) {
        printf("Queue is empty.\n");
        return;
    }

    printf("Queue elements: ");
    for (int i = queue->front; i <= queue->rear; i++) {
        printf("%d ", queue->array[i]);
    }
    printf("\n");
}

int main() {
    struct Queue myQueue;
    initializeQueue(&myQueue);

    enqueue(&myQueue, 10);
    enqueue(&myQueue, 20);
    enqueue(&myQueue, 30);

    displayQueue(&myQueue);

    printf("Dequeued element: %d\n", dequeue(&myQueue));
    printf("Dequeued element: %d\n", dequeue(&myQueue));

    displayQueue(&myQueue);

    return 0;
}
